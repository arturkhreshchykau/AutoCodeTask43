﻿using System;

namespace AutocodeTask43
{
    public class Class1
    {
        static void Main(string[] args)
        {
            Class1 class1 = new Class1();
            class1.DoSomething("dasdasd");
        }

        public int DoSomething(string msg)
        {
            if (msg is null)
            {
                throw new ArgumentNullException(nameof(msg));
            }

            return 10;
        }
    }
}
